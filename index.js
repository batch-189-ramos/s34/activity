// Express Setup
const express = require('express')

const app = express()

const port = 4000

// Middleware
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Routes

// Home route
app.get('/home', (req, res) => {
	res.send('Welcome to the homepage!')
})


// Get Users route

let users = [
	{
		username: 'johndoe',
		password: 'johndoe1234'
	}
]

app.get('/users', (req, res) => {
	res.send(users)
})


// Delete user route

app.delete('/delete-user', (req, res) => {
	let message

	for( let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username && req.body.password == users[i].password){
			message = `User ${req.body.username} has been deleted.`
			break
		} else {
			message = `Username doen not exist.`
		}
	}
	res.send(message)
})


app.listen(port, () => console.log(`Server is running at local host: ${port}`))